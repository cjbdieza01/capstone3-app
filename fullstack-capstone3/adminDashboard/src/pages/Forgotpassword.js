import React from 'react';
import CustomInput from "../components/CustomInput";

const Login = () => {
  return (
    <div className="d-flex align-items-center justify-content-center" style={{ background: "#ffd255", minHeight: "100vh"}}>
      <div className="w-25 bg-white rounded-3 p-4" style={{ position: "absolute", top: "50%", transform: "translateY(-70%)" }}>
        <h3 className="text-center">Forgot Password</h3>
        <p className="text-center">Please enter your registered email.</p>
        <form action="">
          <CustomInput type='text' label="Email Address" id="email" />
          <button
            className="border-0 px-3 py-2 text-white fw-bold w-100"
            style={{ background: "#ffd333" }}
            type='submit'
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
