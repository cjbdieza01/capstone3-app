const { default: mongoose } = require('mongoose');
const dbConnect = () => {
    try {
    const conn = mongoose.connect(process.env.DB_CONNECT);
    console.log("Database is ready!")
    }
    catch (error) {
    console.log('Database connection failed');
    }
}
module.exports = dbConnect;