const Product = require('../models/productModel');
const User = require('../models/userModel')
const asyncHandler = require("express-async-handler");
const slugify = require('slugify');
const validateMongoDbId = require('../utils/validateMongoDbId');


const createProduct = asyncHandler(async (req, res) => {
    try {
        if (req.body.title) {
            req.body.slug = slugify (req.body.title);
        }
        const newProduct = await Product.create(req.body);    
        res.json(newProduct);
    }
    catch (error) {
        throw new Error(error);
    }
});

// Update Product
const updateProduct = asyncHandler(async (req, res) => {
    const id = req.params.id;
    try {
      if (req.body.title) {
        req.body.slug = slugify(req.body.title);
      }
      const updatedProduct = await Product.findByIdAndUpdate(id, req.body, {
        new: true,
      });
      if (!updatedProduct) {
        throw new Error('Product not found');
      }
      res.json(updatedProduct);
    } catch (error) {
      res.status(404).json({ error: error.message });
    }
  });
  
const deleteProduct = asyncHandler(async (req, res) => {
  const id = req.params.id;
  try {
    const deletedProduct = await Product.findByIdAndDelete(id);
    if (deletedProduct) {
      res.json(deletedProduct);
    } else {
      res.status(404).json({ message: "Product not found!" });
    }
  } catch (error) {
    throw new Error(error);
  }
});

const getProduct = asyncHandler(async (req, res) => {
    const {id} = req.params;
    try {
        const findProduct = await Product.findById(id);
        res.json(findProduct);
    }
    catch (error) {
        throw new Error(error);
    }
});

const getAllProduct = asyncHandler(async (req, res) => {
    try {
        // Filter
        const queryObj = {...req.query};
        const excludeFields = ["page", "sort", "limit", "fields"]
        excludeFields.forEach((el) => delete queryObj[el]);
        console.log(queryObj);
        let queryStr = JSON.stringify(queryObj);
        queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);
        let query = Product.find(JSON.parse(queryStr))
        // Sorting  
        if (req.query.sort) {
            const sortBy = req.query.sort.split(',').join(" ")
            query = query.sort(sortBy);
        } else {
            query = query.sort('-createdAt');
        }
        // Limiting Fields
        if (req.query.fields) {
            const fields = req.query.fields.split(",").join(" ");
            query = query.select(fields)
        } else {
            query = query.select('-__v')
        }
        // Page
        const page = req.query.page;
        const limit = req.query.limit;
        const skip = (page - 1) * limit;
        query = query.skip(skip).limit(limit);
        if (req.query.page) {
            const productCount = await Product.countDocuments();
            if (skip >= productCount) throw new Error("This Page does not exist")
        }
        console.log(page, limit, skip);

        const product = await query
        res.json(product);
    } catch (error) {
        throw new Error(error);
    }
});

const addToWishList = asyncHandler (async (req, res) => {
  const {_id} = req.user;
  const {productId} = req.body;
  try {
    const user = await User.findById(_id);
    const alreadyAdded = user.wishlist.find((id) => id.toString() === productId);
    if (alreadyAdded) {
      let user = await User.findByIdAndUpdate(_id, {
        $pull: {wishlist: productId},
      }, {new: true,});
      res.json(user)
    } else {
      let user = await User.findByIdAndUpdate(_id, {
        $push: {wishlist: productId},
      }, {new: true,});
      res.json(user)
    }
  } catch (error) {
    throw new Error(error)
  }
})

const rating = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { star, prodId, } = req.body;
    try {
    const product = await Product.findById(prodId);
    let alreadyRated = product.ratings.find(
      (userId) => userId.postedby.toString() === _id.toString()
    );
    if (alreadyRated) {
      const updateRating = await Product.updateOne(
        { ratings: { $elemMatch: alreadyRated } },
        { $set: { "ratings.$.star": star, "ratings.$.comment": comment } },
        { new: true }
      );
      res.json(updateRating); // Return the update result if needed
    } else {
      const rateProduct = await Product.findByIdAndUpdate(
        prodId,
        {
          $push: {
            ratings: {
              star: star,
              // comment: comment,
              postedby: _id,
            },
          },
        },
        { new: true }
      );
      res.json(rateProduct); // Return the updated product document if needed
    }
  } catch (error) {
    // Handle the error appropriately (e.g., send error response, log error, etc.)
    throw new Error(error);
  }
});


module.exports = 
{   
    createProduct, 
    getProduct,
    getAllProduct,
    updateProduct,
    deleteProduct,
    addToWishList,
    rating
};