const Category = require('../models/blogCategoryModel');
const asyncHandler = require("express-async-handler");
const validateMongoDbId = require("../utils/validateMongoDbId");
const { findById } = require('../models/productCategoryModel');

const createBlogCategory = asyncHandler(async (req, res) => {
  try {
    const newCategory = await Category.create(req.body);
    res.json(newCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const updateBlogCategory = asyncHandler (async (req, res) => {
    const {id} = req.params;
    validateMongoDbId(id);
    try {
        const updatedCategory = await Category.findByIdAndUpdate(id, req.body, {
            new: true,
        })
        res.json(updatedCategory);
    } catch (error) {
        throw new Error(error);
    }
})

const deleteBlogCategory = asyncHandler (async (req, res) => {
    const { id } = req.params;
    validateMongoDbId(id);
    try {
        const deleteCategory = await Category.findById(id);
        if (!deleteCategory) {
            res.status(404).json({ messege: "Category does not exist or has been already deleted! "})
        }
        const deletedCategory = await Category.findByIdAndDelete(id);
        res.json(deletedCategory);
    } 
    
    catch (error) {
        throw new Error(error);
    }
})

const get1BlogCategory = asyncHandler (async (req, res) => {
    const {id} = req.params;
    validateMongoDbId(id);
    try {
        const getCategory = await Category.findById(id);
        res.json(getCategory);
    } catch (error) {
        throw new Error(error);
    }
})

const getAllBlogCategory = asyncHandler (async (req, res) => {    
    try {
        const allCategory = await Category.find();
        res.json(allCategory)
    } catch (error) {
        throw new Error(error);
    } 
})

module.exports = { createBlogCategory,
    updateBlogCategory,
    deleteBlogCategory,
    get1BlogCategory,
    getAllBlogCategory
};