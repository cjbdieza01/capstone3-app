const Category = require('../models/productCategoryModel');
const asyncHandler = require("express-async-handler");
const validateMongoDbId = require("../utils/validateMongoDbId");

const createProductCategory = asyncHandler(async (req, res) => {
  try {
    const newCategory = await Category.create(req.body);
    res.json(newCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const updateProductCategory = asyncHandler (async (req, res) => {
    const {id} = req.params;
    validateMongoDbId(id);
    try {
        const updatedCategory = await Category.findByIdAndUpdate(id, req.body, {
            new: true,
        })
        res.json(updatedCategory);
    } catch (error) {
        throw new Error(error);
    }
})

const deleteProductCategory = asyncHandler (async (req, res) => {
    const { id } = req.params;
    validateMongoDbId(id);
    try {
        const deletedCategory = await Category.findByIdAndDelete(id);
        res.json(deletedCategory);
    } catch (error) {
        throw new Error(error);
    }
})

const get1ProductCategory = asyncHandler (async (req, res) => {
    const {id} = req.params;
    validateMongoDbId(id);
    try {
        const getCategory = await Category.findById(id);
        res.json(getCategory);
    } catch (error) {
        throw new Error(error);
    }
})

const getAllProductCategory = asyncHandler (async (req, res) => {    
    try {
        const allCategory = await Category.find();
        res.json(allCategory)
    } catch (error) {
        throw new Error(error);
    } 
})

module.exports = { createProductCategory,
    updateProductCategory,
    deleteProductCategory,
    get1ProductCategory,
    getAllProductCategory
};