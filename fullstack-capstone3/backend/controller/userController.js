const User = require('../models/userModel');
const asyncHandler = require('express-async-handler');
const {generateToken} = require('../config/jwtToken');
const validateMongoDbId = require("../utils/validateMongoDbId");
const {generateRefreshToken} = require("../config/refreshToken");
const crypto = require('crypto');
const sendEmail = require('../controller/emailController');
const jwt = require('jsonwebtoken');


const createUser = asyncHandler(async(req, res) => {
        const email = req.body.email;
        const findUser = await User.findOne({email: email});
        if(!findUser) {
            const newUser = await User.create(req.body);
            res.json(newUser);
        } else {
            throw new Error('User already exists!')
        }
    }
)
const loginUserCrtl = asyncHandler(async(req, res) => {
    const {email, password} = req.body;
    // this will check if the user exist or not
    const findUser = await User.findOne({email});
    if (findUser && (await findUser.isPasswordMatched(password))) {
        const refreshToken = await generateRefreshToken(findUser?.id);
        const updateUser = await User.findByIdAndUpdate(findUser.id, {
            refreshToken: refreshToken,
        }, {new: true});
        res.cookie('refreshToken', refreshToken, {
            httpOnly: true,
            maxAge: 72 * 60 * 60 * 1000,
        })
        res.json({
            _id: findUser?._id,
            firstname: findUser?.firstName,
            lastname: findUser?.lastName,
            email: findUser?.email,
            mobile: findUser?.mobile,
            token: generateToken(findUser?._id)
        })
    } else {
        throw new Error("Invalid Credentials!");
    }
});

// Get all users
const getAllUser = asyncHandler(async (req, res) => {
    try {
        const getUsers = await User.find();
        res.json(getUsers)
    } catch (error) {
        throw new Error(error);
    }
})

// Get single user
const getUser = asyncHandler(async (req, res) => {
    const {id} = req.params;
    validateMongoDbId(id);
try {
    const getUser = await User.findById(id);
    res.json({
        getUser,
    });
} catch (error) {
     {
        throw new Error(error);
    }
}
});

// Delete a user
const deleteUser = asyncHandler(async (req, res) =>{
    const {id} = req.params;
    validateMongoDbId(_id);
    try {
        const deleteUser = await User.findByIdAndDelete(id);
        res.json({
            deleteUser,
        });
    } catch (error){
        {
            throw new Error(error);
        }
    }
});

// Handle refresh token
const handleRefreshToken = asyncHandler(async (req, res) => {
    const cookie = req.cookies;
    console.log(cookie);
    if (!cookie?.refreshToken) throw new Error('No refresh token in Cookies');
    const refreshToken = cookie.refreshToken;
    console.log(refreshToken);
    const user = await User.findOne({ refreshToken });
    if (!user) throw new Error('No refresh token present in db or not matched');
    jwt.verify(refreshToken, process.env.JWT_SECRET, (err, decoded) => {
        if (err || user.id !== decoded.id) { throw new Error('There is something wrong with the refresh token'); 
    } else {
        const accessToken = generateToken(user?._id)
        res.json({accessToken});
    }
    })
});

// Logout
const logout = asyncHandler(async (req, res) => {
    const cookie = req.cookies;
    if (!cookie?.refreshToken) throw new Error("No Refresh Token in Cookies");
    const refreshToken = cookie.refreshToken;
    const user = await User.findOne({ refreshToken });
    if (!user) {
      res.clearCookie("refreshToken", {
        httpOnly: true,
        secure: true,
      });
      return res.sendStatus(204); // forbidden
    }
    await User.findOneAndUpdate({refreshToken: refreshToken}, {
      refreshToken: "",
    });
    res.clearCookie("refreshToken", {
      httpOnly: true,
      secure: true,
    });
    res.sendStatus(204); // forbidden
  });

// Update User
const updateUser = asyncHandler(async (req, res) => {
    console.log();
    const {_id} = req.user;
    validateMongoDbId(_id);
    try {
        const updateUser = await User.findByIdAndUpdate(_id, {
            firstName: req?.body?.firstName,
            lastName: req?.body?.lastName,
            email: req?.body?.email,
            mobile: req?.body?.mobile
        },{new: true,});
        res.json(updateUser)
    } catch (error) {
        throw new Error(error)
    }
})


const blockUser = asyncHandler(async (req, res) => {
    const {id} = req.params;
    validateMongoDbId(_id);
    try {
        const blockUsr = await User.findByIdAndUpdate(
            id, {isBlocked: true}, {new: true});
            res.json(blockUsr)
    } catch (error) {
        throw new Error(error)
    }
})

const unblockUser = asyncHandler(async (req, res) => {
    const {id} = req.params;
    validateMongoDbId(_id);
    try {
        const unblockUsr = await User.findByIdAndUpdate(
            id, {isBlocked: false}, {new: true});
            res.json(unblockUsr)
    } catch (error) {
        throw new Error(error)
    }
})

const updatePassword = asyncHandler (async (req, res) => {
    const {_id} = req.user;
    const {password} = req.body;
    validateMongoDbId(_id);
    const user = await User.findById(_id);
    if (password) {
        user.password = password;
        const updatePassword = await user.save();
        res.json(updatePassword);
    } else {
        res.json(user);
    }
});

const forgotPasswordToken = asyncHandler (async (req, res) => {
    const {email} = req.body;
    const user = await User.findOne({email});
    if (!user) throw new Error ('User not found with this email');
    try {
        const token = await user.createPasswordResetToken();
        await user.save();
        const resetUrl = `Hi, Please follow this link to reset Your Password. This link is valid in 24 hours from now. <a href='http://localhost:8080/api/user/resetPassword/${token}'>Click here</>`
        const data = {
            to: email,
            text: "Hey User",
            subject: "Forgot Password",
            htm: resetUrl,
        };
        sendEmail(data);
        res.json(token);
    } catch (error) {
        throw new Error(error);
    }
})

const resetPassword = asyncHandler (async (req, res) => {
    const {password} = req.body;
    const {token} = req.params;
    const hashedToken = crypto.createHash('sha256').update(token).digest("hex")
    const user = await User.findOne({
        passwordResetToken: hashedToken,
        passwordResetExpires: {$gt: Date.now () },
    });
    if (!user) 
        throw new Error("Token Expired, Please Try again later");
        user.password = password;
        user.passwordResetToken = undefined,
        user.passwordResetExpires = undefined;
        await user.save();
        res.json(user);
});

module.exports = {
    createUser, 
    loginUserCrtl,
    getAllUser,
    getUser,
    deleteUser,
    updateUser,
    blockUser,
    unblockUser,
    handleRefreshToken,
    logout,
    updatePassword,
    forgotPasswordToken,
    resetPassword

};

function orderPizza(callback) {
    setTimeout(() => {
        const pizza = "pizza"
        callback(pizza)
    }, 2000)
}
function pizzaReady(pizza) {
    console.log(`Eat the ${pizza}`)
}
orderPizza(pizzaReady)
console.log(`Call Qoli`)