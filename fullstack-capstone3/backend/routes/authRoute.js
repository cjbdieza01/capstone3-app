const express = require("express");
const router = express.Router();
const {createUser,
     loginUserCrtl, 
     getAllUser,
     getUser,
     deleteUser,
     updateUser,
     blockUser,
     unblockUser,
     handleRefreshToken,
     logout,
     updatePassword,
     forgotPasswordToken,
     resetPassword
    }
     = require("../controller/userController");
const {authMiddleware, isAdmin} = require('../middlewares/authMiddleware')


router.post('/register', createUser);
router.post('/forgotPasswordToken', forgotPasswordToken )
router.put('/resetPassword/:token', resetPassword);
router.post('/login', loginUserCrtl);
router.put('/updatePassword', authMiddleware, updatePassword)
router.get('/all-users', getAllUser);
router.get("/refresh", handleRefreshToken);
router.get('/logout', logout);
router.get('/:id', authMiddleware, isAdmin, getUser);
router.delete('/:id', deleteUser);
router.put('/edit-user',authMiddleware, updateUser);
router.put('/block-user/:id', authMiddleware, isAdmin, blockUser);
router.put('/unblock-user/:id', authMiddleware, isAdmin, unblockUser);


module.exports = router;