const express = require("express");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const { 
    createBlogCategory, 
    updateBlogCategory, 
    deleteBlogCategory, 
    getAllBlogCategory, 
    get1BlogCategory 
} = require('../controller/blogCategoryController')
const router = express.Router();



router.post('/', authMiddleware, isAdmin, createBlogCategory);
router.put('/:id', authMiddleware, isAdmin, updateBlogCategory);
router.delete('/:id', authMiddleware, isAdmin, deleteBlogCategory )
router.get('/:id', get1BlogCategory);
router.get('/', getAllBlogCategory);

module.exports = router;