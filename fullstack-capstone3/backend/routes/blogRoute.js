const { createBlog, 
    updateBlog,
    getBlog,
    getAllBlogs,
    deleteBlog,
    likeBlog,
    dislikeBlog
         } = require("../controller/blogController");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");

const express = require("express");
const router = express.Router();

router.post('/', authMiddleware, isAdmin, createBlog);
router.put('/updateBlog/:id', authMiddleware, isAdmin, updateBlog);
router.get('/viewBlog/:id', getBlog);
router.get('/getAllBLogs', getAllBlogs);
router.delete('/deleteBlog/:id', authMiddleware, isAdmin, deleteBlog);
router.put('/likes', authMiddleware, likeBlog);
router.put('/dislike', authMiddleware, dislikeBlog );



module.exports = router;