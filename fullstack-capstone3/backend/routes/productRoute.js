const express = require('express');
const router = express.Router();
const {
    createProduct, 
    getProduct,
    getAllProduct,
    updateProduct,
    deleteProduct,
    addToWishList, 
    rating
} = require('../controller/productController')
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware')

router.post('/createProduct',  authMiddleware, isAdmin, createProduct);
router.get('/get-product/:id', getProduct);
router.put('/wishlists', authMiddleware, addToWishList);
router.put('/rating', authMiddleware, rating);
router.put('/updateProduct/:id',  authMiddleware, isAdmin, updateProduct)
router.get('/getAllProduct', getAllProduct);
router.delete('/deleteProduct/:id',  authMiddleware, isAdmin, deleteProduct)


module.exports = router;