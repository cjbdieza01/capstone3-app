const express = require("express");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const { createProductCategory, updateProductCategory, deleteProductCategory, getAllProductCategory, get1ProductCategory }= require('../controller/productCategoryController')
const router = express.Router();



router.post('/', authMiddleware, isAdmin, createProductCategory);
router.put('/:id', authMiddleware, isAdmin, updateProductCategory);
router.delete('/:id', authMiddleware, isAdmin, deleteProductCategory )
router.get('/:id', get1ProductCategory);
router.get('/', getAllProductCategory);

module.exports = router;