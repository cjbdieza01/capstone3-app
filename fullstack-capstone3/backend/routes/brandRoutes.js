const express = require("express");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const { createProductBrand, updateProductBrand, deleteProductBrand, getAllProductBrand, get1ProductBrand }= require('../controller/brandController')
const router = express.Router();



router.post('/', authMiddleware, isAdmin, createProductBrand);
router.put('/:id', authMiddleware, isAdmin, updateProductBrand);
router.delete('/:id', authMiddleware, isAdmin, deleteProductBrand )
router.get('/:id', get1ProductBrand);
router.get('/', getAllProductBrand);

module.exports = router;